"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const prompt_1 = require("./constant/prompt");
const readline = __importStar(require("readline"));
const parkingSpot_1 = __importDefault(require("./models/parkingSpot"));
const config_1 = require("./config");
const vehicle_1 = require("./models/vehicle");
const car_1 = require("./models/car");
const ticket_1 = __importDefault(require("./models/ticket"));
const fs = __importStar(require("fs"));
const logger_1 = __importDefault(require("./logger"));
class App {
    static init() {
        try {
            App.parkingList = { spotList: [], vehicleObj: {}, tickets: {} };
            App.operationPerformed = config_1.Operations.File;
            if (!App.standard_input) {
                App.standard_input = readline.createInterface({
                    input: process.stdin,
                    output: process.stdout,
                    terminal: false
                });
                App.standard_input.on('line', App.start);
            }
            //App.start(1)
            //console.log(QUESTIONS.startWith);
        }
        catch (err) {
            logger_1.default.log('init', 'index.ts', err, {});
        }
    }
    static start(data) {
        const inputObj = new Set();
        data.split(" ").forEach((ele) => {
            if (!inputObj.has(ele)) {
                inputObj.add(ele);
            }
        });
        if (App.operationPerformed === config_1.Operations.File && !inputObj.has(prompt_1.KEY_WORD.BACK)) {
            App.readFileInput(data);
            return;
        }
        try {
            switch (true) {
                case data === "1":
                    if (App.operationPerformed === config_1.Operations.Input) {
                        console.log(prompt_1.APP_CONSTANT.COLORS.FgRed, 'Type back, to enter into the file read input');
                    }
                    else {
                        App.operationPerformed = config_1.Operations.File;
                        console.log(prompt_1.QUESTIONS.fileName);
                    }
                    break;
                case data === "2":
                    // if(App.operationPerformed !== Operations.File)
                    App.operationPerformed = config_1.Operations.Input;
                    console.log(prompt_1.QUESTIONS.startWithShell);
                    break;
                case !!inputObj.has(prompt_1.KEY_WORD.CREATE_PARKING_LOT):
                    App.operationPerformed = config_1.Operations.Input;
                    App.createParkingLot(data);
                    break;
                case !!inputObj.has(prompt_1.KEY_WORD.PARK):
                    App.operationPerformed = config_1.Operations.Input;
                    App.parkVehicle(data);
                    break;
                case !!inputObj.has(prompt_1.KEY_WORD.REGISTRATION_NUMBERS_FOR_CARS_WITH_COLOUR):
                    App.operationPerformed = config_1.Operations.Input;
                    App.vehicleByColor(data);
                    break;
                case !!inputObj.has(prompt_1.KEY_WORD.SLOT_NUMBERS_FOR_CARS_WITH_COLOUR):
                    App.operationPerformed = config_1.Operations.Input;
                    App.vehicleSlotByColor(data);
                    break;
                case !!inputObj.has(prompt_1.KEY_WORD.SLOT_NUMBER_FOR_REGISTRATION_NUMBER):
                    App.operationPerformed = config_1.Operations.Input;
                    App.vehicleByLicence(data);
                    break;
                case !!inputObj.has(prompt_1.KEY_WORD.LEAVE):
                    App.operationPerformed = config_1.Operations.Input;
                    App.unparkVehicle(data);
                    break;
                case !!inputObj.has(prompt_1.KEY_WORD.STATUS):
                    App.operationPerformed = config_1.Operations.Input;
                    App.getStatus(data);
                    break;
                case !!inputObj.has(prompt_1.KEY_WORD.EXIT):
                    console.log(prompt_1.APP_CONSTANT.COLORS.BgCyan, prompt_1.REPLY.thankYou);
                    process.exit();
                    break;
                case !!inputObj.has(prompt_1.KEY_WORD.BACK):
                    App.init();
                    break;
                default:
                    console.log(prompt_1.APP_CONSTANT.COLORS.FgRed, prompt_1.REPLY.cmdNotMatch);
                    console.log('App.operationPerformed', App.operationPerformed);
                    if (App.operationPerformed === config_1.Operations.File) {
                        console.log(prompt_1.QUESTIONS.fileName);
                    }
                    else {
                        if (App.operationPerformed === config_1.Operations.Input)
                            console.log(prompt_1.QUESTIONS.startWithShell);
                        else {
                            console.log(prompt_1.QUESTIONS.startWith);
                        }
                    }
                    break;
            }
        }
        catch (err) {
            logger_1.default.log('start', 'index.ts', err, data);
        }
    }
    static processFile(file_name) {
        try {
            const inputs = [];
            App.operationPerformed = null;
            console.log(`${prompt_1.REPLY.readingFile(file_name)}`);
            const readInterface = readline.createInterface({
                input: fs.createReadStream(`${prompt_1.APP_CONSTANT.FILE_PATH}${file_name}`),
                output: process.stdout,
                terminal: false
            });
            readInterface.on('line', (line) => {
                inputs.push(line);
            });
            readInterface.on('close', () => {
                inputs.forEach((ele) => {
                    App.start(ele);
                });
            });
        }
        catch (err) {
            logger_1.default.log('processFile', 'index.ts', err, file_name);
        }
    }
    static readFileInput(data) {
        try {
            const [file_name] = data.split(" ");
            if (!file_name) {
                console.log(prompt_1.APP_CONSTANT.COLORS.FgRed, prompt_1.REPLY.fileNameNotEntered);
                return;
            }
            fs.exists(`${prompt_1.APP_CONSTANT.FILE_PATH}${file_name}`, (exist) => {
                if (exist) {
                    App.processFile(file_name);
                }
                else {
                    console.log(prompt_1.APP_CONSTANT.COLORS.FgRed, prompt_1.REPLY.fileNotExist(file_name));
                }
            });
        }
        catch (err) {
            logger_1.default.log('readFileInput', 'index.ts', err, data);
            App.operationPerformed = null;
        }
    }
    static createParkingLot(data) {
        try {
            const isAlreadyCreatd = App.parkingList.spotList.length;
            const [word, numSpotLoc] = data.split(" ");
            let numSpot = Number(numSpotLoc);
            if (isNaN(numSpot) || numSpot < 1) {
                console.log(prompt_1.APP_CONSTANT.COLORS.FgRed, prompt_1.REPLY.incorrectSlot);
                return;
            }
            for (let i = 1; i <= numSpot; i++) {
                App.parkingList.spotList.push(new parkingSpot_1.default(i, config_1.ParkingLevel.One, true, vehicle_1.VehicleSize.Car));
            }
            if (isAlreadyCreatd > 1) {
                console.log(prompt_1.APP_CONSTANT.COLORS.FgGreen, `${prompt_1.REPLY.parkingCreated(numSpot)}${prompt_1.REPLY.extraSlot}`);
            }
            else {
                App.availableSpot = 1;
                console.log(prompt_1.APP_CONSTANT.COLORS.FgGreen, prompt_1.REPLY.parkingCreated(numSpot));
            }
            //App.start("2");
        }
        catch (err) {
            logger_1.default.log('createParkingLot', 'index.ts', err, data);
        }
    }
    static findNextParkingSpot(spot) {
        try {
            for (let i = spot + 1; i < App.parkingList.spotList.length; i++) {
                if (!App.parkingList.spotList[i].getVehicleDetails() && App.parkingList.spotList[i].status()) {
                    return i;
                }
            }
            return spot;
        }
        catch (err) {
            logger_1.default.log('findNextParkingSpot', 'index.ts', err, spot);
        }
    }
    static parkVehicle(data) {
        try {
            const [word, licence, color] = data.split(" ");
            if (App.parkingList.vehicleObj[licence]) {
                console.log(prompt_1.APP_CONSTANT.COLORS.FgRed, `${prompt_1.REPLY.vehicleAlreadyReg(licence)}`);
            }
            else {
                if (App.availableSpot >= App.parkingList.spotList.length) {
                    console.log(prompt_1.APP_CONSTANT.COLORS.FgRed, `${prompt_1.REPLY.noAvailableSpot}`);
                }
                else {
                    const tempSpot = App.availableSpot;
                    const spotStatus = App.parkingList.spotList[App.availableSpot].status();
                    //check available spot is active spot or not
                    if (!spotStatus) {
                        App.availableSpot = App.findNextParkingSpot(App.availableSpot);
                    }
                    if (!spotStatus && App.availableSpot === tempSpot) {
                        //in case , if there is no active spot
                        console.log(prompt_1.APP_CONSTANT.COLORS.FgRed, `${prompt_1.REPLY.noActiveSpot}`);
                    }
                    else {
                        const spot = App.parkingList.spotList[App.availableSpot];
                        const veh = new car_1.Car(licence, vehicle_1.VehicleSize.Car, color, spot);
                        const ticketId = new Date().getTime();
                        const ticket = new ticket_1.default(ticketId, config_1.PaymentMode.Cash, 100, new Date().getTime(), App.availableSpot, veh);
                        App.parkingList.tickets[ticketId] = ticket;
                        spot.setVehicle(veh);
                        spot.setTicket(ticketId);
                        App.parkingList.vehicleObj[licence] = veh;
                        console.log(`${prompt_1.REPLY.vehicleReg(App.availableSpot)}`);
                        App.availableSpot = App.findNextParkingSpot(App.availableSpot);
                        if (App.availableSpot === tempSpot) {
                            App.availableSpot = App.parkingList.spotList.length;
                        }
                    }
                }
            }
            //App.start("2");
        }
        catch (err) {
            logger_1.default.log('parkVehicle', 'index.ts', err, data);
        }
    }
    static vehicleByColor(data) {
        try {
            let [word, color] = data.split(" ");
            color = color.toLowerCase();
            const veh = [];
            Object["values"](App.parkingList.vehicleObj).forEach((ele) => {
                if (ele.getColor() === color) {
                    veh.push(ele.getLicense());
                }
            });
            console.log(veh.join(","));
            //App.start("2");
        }
        catch (err) {
            logger_1.default.log('vehicleByColor', 'index.ts', err, data);
        }
    }
    static vehicleSlotByColor(data) {
        try {
            let [word, color] = data.split(" ");
            color = color.toLowerCase();
            const veh = [];
            Object["values"](App.parkingList.vehicleObj).forEach((ele) => {
                if (ele.getColor() === color && ele.getParkingSpot()) {
                    veh.push(ele.getParkingSpot().getSpot());
                }
            });
            console.log(veh.join(","));
            //App.start("2");
        }
        catch (err) {
            logger_1.default.log('vehicleSlotByColor', 'index.ts', err, data);
        }
    }
    static vehicleByLicence(data) {
        try {
            let [word, licence] = data.split(" ");
            const veh = App.parkingList.vehicleObj[licence];
            if (!veh) {
                console.log(prompt_1.APP_CONSTANT.COLORS.FgRed, prompt_1.REPLY.notFound);
                return;
            }
            const spot = veh.getParkingSpot();
            if (spot) {
                console.log(spot.getSpot());
            }
            else {
                console.log(prompt_1.APP_CONSTANT.COLORS.FgRed, prompt_1.REPLY.spotNotAssociated);
            }
            //App.start("2");
        }
        catch (err) {
            logger_1.default.log('vehicleByLicence', 'index.ts', err, data);
        }
    }
    static unparkVehicle(data) {
        try {
            const [word, num] = data.split(" ");
            const spot = Number(num);
            if (isNaN(spot)) {
                console.log(prompt_1.APP_CONSTANT.COLORS.FgRed, prompt_1.REPLY.spotNotNumber);
                return;
            }
            if (!App.parkingList.spotList[spot]) {
                console.log(prompt_1.APP_CONSTANT.COLORS.FgRed, prompt_1.REPLY.spotNotExist(spot));
                return;
            }
            const veh = App.parkingList.spotList[spot].getVehicleDetails();
            if (veh) {
                App.parkingList.spotList[spot].setVehicle(null);
                const ticketId = App.parkingList.spotList[spot].getTicket();
                App.parkingList.spotList[spot].setTicket(null);
                App.parkingList.tickets[ticketId].setTicketStatus(true);
                veh.setParkingSpot(null);
                App.availableSpot = spot < App.availableSpot ? spot : App.availableSpot;
                console.log(prompt_1.APP_CONSTANT.COLORS.FgMagenta, prompt_1.REPLY.slotAvailable(spot));
            }
            else {
                console.log(prompt_1.REPLY.slotAlreadyEmpty(spot));
            }
        }
        catch (err) {
            logger_1.default.log('unparkVehicle', 'index.ts', err, data);
        }
    }
    static getStatus(data) {
        try {
            const list = [];
            let veh = null;
            for (let i = 1; i < App.parkingList.spotList.length; i++) {
                veh = App.parkingList.spotList[i].getVehicleDetails();
                list.push({
                    slot: i,
                    licence: veh ? veh.getLicense() : 'Available',
                    color: veh ? veh.getColor() : 'Available',
                });
            }
            console.log(prompt_1.REPLY.printStatus(list));
        }
        catch (err) {
            logger_1.default.log('getStatus', 'index.ts', err, data);
        }
    }
}
App.standard_input = null;
App.parkingList = null;
App.operationPerformed = null;
App.init();
