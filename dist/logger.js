"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const prompt_1 = require("./constant/prompt");
class Logger {
    static log(fn, file, err, args) {
        console.log(prompt_1.APP_CONSTANT.COLORS.FgMagenta, `Don't get Panic.Kindly contact  the Developer @niravkapoor27@gmail.com`);
        console.log(prompt_1.APP_CONSTANT.COLORS.FgRed, `Method : ${fn}, File : ${file}, error: ${JSON.stringify(err)}, argument : ${JSON.stringify(args)}`);
    }
}
exports.default = Logger;
